function Tuple(left, right) {
  return { left, right }
}

function left(tuple) {
  return tuple.left
}

function right(tuple) {
  return tuple.right
}
