function pipe(fns) {
  return function(data) {
    if (isEmpty(fns)) {
      return data
    }
    return pipe(tail(fns))(head(fns)(data))
  }
}
