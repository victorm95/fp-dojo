const Nothing = Symbol('Nothing')

function Maybe(data) {
  return { data }
}
