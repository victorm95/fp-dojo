const Nil = Symbol('Nil')

function List(...vals) {
  let list = Nil
  for (let val of vals.reverse()) {
    list = cons(val, list)
  }
  return list
}

function cons(head, tail) {
  return { head, tail }
}

function head(list) {
  return list.head
}

function tail(list) {
  return list.tail
}

function isEmpty(list) {
  return list === Nil
}

function show(list) {
  if (isEmpty(list)) {
    return 'Nil'
  }
  return String(head(list)) + ' :: ' + show(tail(list))
}

function map(fn, list) {
  if (isEmpty(list)) {
    return list
  }
  return cons(fn(head(list)), map(fn, tail(list)))
}

function each(fn, list) {
  if (isEmpty(list)) {
    return
  }
  fn(head(list))
  each(fn, tail(list))
}

function fold(fn, acc, list) {
  if (isEmpty(list)) {
    return acc
  }
  return fold(fn, fn(acc, head(list)), tail(list))
}

function last(list) {
  if (isEmpty(list)) {
    throw new Error('No last element in an empty list')
  }
  if (isEmpty(tail(list))) {
    return head(list)
  }
  return last(tail(list))
}

function init(list) {
  if (isEmpty(list)) {
    return list
  }
  if (isEmpty(tail(list))) {
    return Nil
  }
  return cons(head(list), init(tail(list)))
}

function reverse(list) {
  if (isEmpty(list)) {
    return list
  }
  return cons(last(list), reverse(init(list)))
}

function take(amount, list) {
  if (amount === 0 || isEmpty(list)) {
    return Nil
  }
  return cons(head(list), take(amount - 1, tail(list)))
}

function skip(amount, list) {
  if (amount === 0 || isEmpty(list)) {
    return list
  }
  return skip(amount - 1, tail(list))
}

function size(list) {
  if (isEmpty(list)) {
    return 0
  }
  return size(tail(list)) + 1
}

function filter(fn, list) {
  if (isEmpty(list)) {
    return list
  }
  if (fn(head(list))) {
    return cons(head(list), filter(fn, tail(list)))
  }
  return filter(fn, tail(list))
}
